
import 'dart:io';

import 'package:aes_crypt/aes_crypt.dart';
import 'package:dio/dio.dart';
import 'package:epub_viewer/epub_viewer.dart';
import 'package:ext_storage/ext_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:flutter_file_manager/flutter_file_manager.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:path_provider_ex/path_provider_ex.dart';
import 'package:struturenew/Ui/download_page/bloc/download_bloc.dart';
import 'package:struturenew/Ui/download_page/bloc/download_event.dart';
import 'package:struturenew/Ui/download_page/bloc/download_state.dart';
//import 'package:fluttertoast/fluttertoast.dart';

import 'package:struturenew/core/base_widget/base_click.dart';
import 'package:struturenew/core/base_widget/base_text.dart';
import 'package:struturenew/core/constent.dart';
import 'package:struturenew/core/style/base_color.dart';
import 'package:struturenew/core/style/custom_loader.dart';
import 'package:struturenew/model/download_model/download_model.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../../injectoin.dart';
import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:epub_viewer/epub_viewer.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
class DownloadPage extends StatefulWidget {

  @override
  _DownloadPageState createState() => _DownloadPageState();
}

class _DownloadPageState extends State<DownloadPage> {

  var _listController = ScrollController();
  final _bloc = sl<DownloadBloc>();
  TextEditingController _url = TextEditingController();
String savePath;
  String encryptionpath;
  var decryptionPath;
  @override
  void initState() {
    //_bloc.add(GetData((b)=>b..id  = widget.instructor.id));
    _url.text="http://almashreqebookstore.com/books/4.epub";
getPermission();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(bloc: _bloc,builder: (BuildContext context , DownloadState state){
      error(state.error);
      if(state.success){

        File file = new File(savePath);
        var deleted = file.delete();
        print("deleted: "+deleted.toString());
        _bloc.add(ChangeStatus());
      }


      return Scaffold(
          appBar: new AppBar(
            elevation: 0,
            backgroundColor: darkBlue,
            centerTitle: false,
            automaticallyImplyLeading: true,
            leading: GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Icon(
                Icons.arrow_back,
                color: Colors.white,
              ),
            ),
            title: Padding(
              padding: const EdgeInsets.only(left: 0.0),
              child: baseText(
                  title:  "Download File" ,
                  color: Colors.white,
                  size: 15.0,
                  fontFamily: "SegoeUIRegular"),
            ),
          ),
          body:
          Stack(
            children: [
              SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 30.0, left: 30, right: 30),
                      child: Container(
                        width: double.infinity,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          border: Border.all(color: textColor, width: 1),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 8.0, top: 8),
                          child: TextFormField(
                            keyboardType: TextInputType.text,
                            controller: _url,
                            style: TextStyle(
                                color: textColor, fontFamily: "SegoeUIRegular"),
                            decoration: InputDecoration(
                                contentPadding: EdgeInsets.only(
                                  left: 10,
                                ),
                                focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        width: 2.0, color: Colors.transparent)),
                                hintText:
                                "Enter Url" ,
                                hintStyle: TextStyle(color: textColor, fontSize: 15),
                                labelStyle: TextStyle(
                                    fontFamily: "SegoeUIRegular",
                                    fontSize: 15.0,
                                    fontWeight: FontWeight.normal,
                                    color: textColor),
                                enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        width: 1.0, color: Colors.transparent))),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsetsDirectional.only(
                          start: 20.0, top: 20.0, end: 20),
                      child: baseClick(
                          title: "DOWNLOAD",
                          color: darkBlue,
                          radius: 5,
                          height: 50,
                          width: double.infinity,
                          onTap: ()async {
                            // setState(() {
                            //  downloading=true;
                            // });
                            String full;
                            String externalDirectoryPath =
                            await ExtStorage.getExternalStorageDirectory();
                            var d= await new Directory(externalDirectoryPath+'/downloadepub').create()
                            // The created directory is returned as a Future.
                                .then((Directory directory) async {
                              print(directory.path);
                              full=_url.text.split("/").last;
                              fullPath= await directory.path+"/"+full;
                            });
                            _bloc.add(Download((b) => b
                              ..dio = dio
                              ..url =_url.text
                              ..savePath=fullPath
                            ));
                            print("hhhhhh"+state.success.toString());


                            // if(progressString=="100%") {
                            //   setState(() {
                            //     downloading = false;
                            //   });
                            // }

                            setState(() {
                              savePath=fullPath;
                            });



                          }),
                    ),
                    Padding(
                      padding: const EdgeInsetsDirectional.only(
                          start: 20.0, top: 20.0, end: 20),
                      child: baseClick(
                          title: "OPEN",
                          color: darkBlue,
                          radius: 5,
                          height: 50,
                          width: double.infinity,
                          onTap: ()async {


                            decrypt_file(state.encryptedPath);



                            Directory appDocDir =
                            await getApplicationDocumentsDirectory();
                            print('$appDocDir');

                            String iosBookPath = '${appDocDir.path}/chair.epub';
                            print(iosBookPath);
                            String androidBookPath =decryptionPath;
                            EpubViewer.setConfig(
                                themeColor: Theme.of(context).primaryColor,
                                identifier: "iosBook",
                                scrollDirection: EpubScrollDirection.ALLDIRECTIONS,
                                allowSharing: true,
                                enableTts: true,
                                nightMode: true);
                            EpubViewer.open(
                              androidBookPath,
                              //Platform.isAndroid ? androidBookPath : iosBookPath,
                              lastLocation: EpubLocator.fromJson({
                                "bookId": "2239",
                                "href": "/OEBPS/ch06.xhtml",
                                "created": 1539934158390,
                                "locations": {
                                  "cfi": "epubcfi(/0!/4/4[simple_book]/2/2/6)"
                                }
                              }),
                            );

                            // await EpubViewer.openAsset(
                            //   'assets/4.epub',
                            //   lastLocation: EpubLocator.fromJson({
                            //     "bookId": "2239",
                            //     "href": "/OEBPS/ch06.xhtml",
                            //     "created": 1539934158390,
                            //     "locations": {
                            //       "cfi": "epubcfi(/0!/4/4[simple_book]/2/2/6)"
                            //     }
                            //   }),
                            // );
                            // get current locator
                            EpubViewer.locatorStream.listen((locator) {
                              print(
                                  'LOCATOR: ${EpubLocator.fromJson(jsonDecode(locator))}');
                              File filee = new File(decryptionPath);
                              var deletedd = filee.delete();
                              print("deleted: "+deletedd.toString());

                            });



                          }),
                    ),

                  ],
                ),
              ),
              state.isLoading?Center(child: loader(context: context),):Container()
            ],
          )
         );

    });

  }

  // Future  encrypt_file(String path) async {
  //   AesCrypt crypt = AesCrypt();
  //   crypt.setOverwriteMode(AesCryptOwMode.on);
  //   crypt.setPassword('MyPassword1234');
  //   String encFilepath;
  //   try {
  //     encFilepath = crypt.encryptFileSync(path);
  //     // setState(() {
  //     //   encryptionpath=encFilepath;
  //     // });
  //     encryptionpath=await encFilepath;
  //     print('The encryption has been completed successfully.');
  //     print('Encrypted file: $encFilepath');
  //     error('Encrypted file: $encFilepath');
  //   } catch (e) {
  //     if (e.type == AesCryptExceptionType.destFileExists) {
  //       print('The encryption has been completed unsuccessfully.');
  //       print(e.message);
  //     } else {
  //       return 'ERROR';
  //     }
  //   }
  //   return "success";
  // }
  String decrypt_file(String path) {
    AesCrypt crypt = AesCrypt();
    crypt.setOverwriteMode(AesCryptOwMode.on);
    crypt.setPassword('MyPassword1234');
    String decFilepath;
    print(path);
    try {
      decFilepath = crypt.decryptFileSync(path);
      print('The decryption has been completed successfully.');
      error("completed successfully.");
      print('Decrypted file 1: $decFilepath');
      print('File content: ' + File(decFilepath).path);
      setState(() {
        decryptionPath=File(decFilepath).path;
      });
      print("dec"+decryptionPath);



    } catch (e) {
      if (e.type == AesCryptExceptionType.destFileExists) {
        print('The decryption has been completed unsuccessfully.');
        error("The decryption has been completed unsuccessfully.");
        print(e.message);
      }
      else{
        return 'ERROR';
      }
    }
    return decFilepath;
  }
 Future cretateDirectory(){

 }


  void error(String errorCode) {
    if (errorCode.isNotEmpty) {
      Fluttertoast.showToast(
          msg: errorCode,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor:blueColor,
          textColor: Colors.white,
          fontSize: 16.0);
      _bloc.add(ClearError());
    }
  }
  void getPermission() async {
    print("getPermission");
    await PermissionHandler().requestPermissions([PermissionGroup.storage]);
  }
  var files;

  Future getFiles() async { //asyn function to get list of files

    try {
      final file = File(savePath);

      // Read the file
      return await file.readAsBytes();
    } catch (e) {
      // Return null if we encounter an error
      return null;
    }
  }


}
