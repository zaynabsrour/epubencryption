library download_state;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'download_state.g.dart';

abstract class DownloadState implements Built<DownloadState, DownloadStateBuilder> {
  // fields go here




  String get error;

  bool get isLoading;
  bool get success;
  bool get successEncr;
  bool get successdec;
  @nullable
  String get encryptedPath;
  @nullable
  String get decryPath;





  DownloadState._();

  factory DownloadState([updates(DownloadStateBuilder b)]) = _$DownloadState;

  factory DownloadState.initail() {
    return DownloadState((b) => b
      ..error = ""
      ..isLoading = false
        ..success=false
        ..successEncr=false
        ..successdec=false
        ..encryptedPath=""
        ..decryPath=""


    );
  }
}