// GENERATED CODE - DO NOT MODIFY BY HAND

part of download_event;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$Download extends Download {
  @override
  final Dio dio;
  @override
  final String url;
  @override
  final String savePath;

  factory _$Download([void Function(DownloadBuilder) updates]) =>
      (new DownloadBuilder()..update(updates)).build();

  _$Download._({this.dio, this.url, this.savePath}) : super._() {
    if (dio == null) {
      throw new BuiltValueNullFieldError('Download', 'dio');
    }
    if (url == null) {
      throw new BuiltValueNullFieldError('Download', 'url');
    }
    if (savePath == null) {
      throw new BuiltValueNullFieldError('Download', 'savePath');
    }
  }

  @override
  Download rebuild(void Function(DownloadBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  DownloadBuilder toBuilder() => new DownloadBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Download &&
        dio == other.dio &&
        url == other.url &&
        savePath == other.savePath;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, dio.hashCode), url.hashCode), savePath.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Download')
          ..add('dio', dio)
          ..add('url', url)
          ..add('savePath', savePath))
        .toString();
  }
}

class DownloadBuilder implements Builder<Download, DownloadBuilder> {
  _$Download _$v;

  Dio _dio;
  Dio get dio => _$this._dio;
  set dio(Dio dio) => _$this._dio = dio;

  String _url;
  String get url => _$this._url;
  set url(String url) => _$this._url = url;

  String _savePath;
  String get savePath => _$this._savePath;
  set savePath(String savePath) => _$this._savePath = savePath;

  DownloadBuilder();

  DownloadBuilder get _$this {
    if (_$v != null) {
      _dio = _$v.dio;
      _url = _$v.url;
      _savePath = _$v.savePath;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Download other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Download;
  }

  @override
  void update(void Function(DownloadBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Download build() {
    final _$result =
        _$v ?? new _$Download._(dio: dio, url: url, savePath: savePath);
    replace(_$result);
    return _$result;
  }
}

class _$DownloadEncr extends DownloadEncr {
  @override
  final Dio dio;
  @override
  final String url;
  @override
  final String savePath;

  factory _$DownloadEncr([void Function(DownloadEncrBuilder) updates]) =>
      (new DownloadEncrBuilder()..update(updates)).build();

  _$DownloadEncr._({this.dio, this.url, this.savePath}) : super._() {
    if (dio == null) {
      throw new BuiltValueNullFieldError('DownloadEncr', 'dio');
    }
    if (url == null) {
      throw new BuiltValueNullFieldError('DownloadEncr', 'url');
    }
    if (savePath == null) {
      throw new BuiltValueNullFieldError('DownloadEncr', 'savePath');
    }
  }

  @override
  DownloadEncr rebuild(void Function(DownloadEncrBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  DownloadEncrBuilder toBuilder() => new DownloadEncrBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is DownloadEncr &&
        dio == other.dio &&
        url == other.url &&
        savePath == other.savePath;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, dio.hashCode), url.hashCode), savePath.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('DownloadEncr')
          ..add('dio', dio)
          ..add('url', url)
          ..add('savePath', savePath))
        .toString();
  }
}

class DownloadEncrBuilder
    implements Builder<DownloadEncr, DownloadEncrBuilder> {
  _$DownloadEncr _$v;

  Dio _dio;
  Dio get dio => _$this._dio;
  set dio(Dio dio) => _$this._dio = dio;

  String _url;
  String get url => _$this._url;
  set url(String url) => _$this._url = url;

  String _savePath;
  String get savePath => _$this._savePath;
  set savePath(String savePath) => _$this._savePath = savePath;

  DownloadEncrBuilder();

  DownloadEncrBuilder get _$this {
    if (_$v != null) {
      _dio = _$v.dio;
      _url = _$v.url;
      _savePath = _$v.savePath;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(DownloadEncr other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$DownloadEncr;
  }

  @override
  void update(void Function(DownloadEncrBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$DownloadEncr build() {
    final _$result =
        _$v ?? new _$DownloadEncr._(dio: dio, url: url, savePath: savePath);
    replace(_$result);
    return _$result;
  }
}

class _$ClearError extends ClearError {
  factory _$ClearError([void Function(ClearErrorBuilder) updates]) =>
      (new ClearErrorBuilder()..update(updates)).build();

  _$ClearError._() : super._();

  @override
  ClearError rebuild(void Function(ClearErrorBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ClearErrorBuilder toBuilder() => new ClearErrorBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ClearError;
  }

  @override
  int get hashCode {
    return 507656265;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('ClearError').toString();
  }
}

class ClearErrorBuilder implements Builder<ClearError, ClearErrorBuilder> {
  _$ClearError _$v;

  ClearErrorBuilder();

  @override
  void replace(ClearError other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ClearError;
  }

  @override
  void update(void Function(ClearErrorBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ClearError build() {
    final _$result = _$v ?? new _$ClearError._();
    replace(_$result);
    return _$result;
  }
}

class _$ChangeStatus extends ChangeStatus {
  factory _$ChangeStatus([void Function(ChangeStatusBuilder) updates]) =>
      (new ChangeStatusBuilder()..update(updates)).build();

  _$ChangeStatus._() : super._();

  @override
  ChangeStatus rebuild(void Function(ChangeStatusBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ChangeStatusBuilder toBuilder() => new ChangeStatusBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ChangeStatus;
  }

  @override
  int get hashCode {
    return 20091075;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('ChangeStatus').toString();
  }
}

class ChangeStatusBuilder
    implements Builder<ChangeStatus, ChangeStatusBuilder> {
  _$ChangeStatus _$v;

  ChangeStatusBuilder();

  @override
  void replace(ChangeStatus other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ChangeStatus;
  }

  @override
  void update(void Function(ChangeStatusBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ChangeStatus build() {
    final _$result = _$v ?? new _$ChangeStatus._();
    replace(_$result);
    return _$result;
  }
}

class _$Encryption extends Encryption {
  @override
  final String path;

  factory _$Encryption([void Function(EncryptionBuilder) updates]) =>
      (new EncryptionBuilder()..update(updates)).build();

  _$Encryption._({this.path}) : super._();

  @override
  Encryption rebuild(void Function(EncryptionBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  EncryptionBuilder toBuilder() => new EncryptionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Encryption && path == other.path;
  }

  @override
  int get hashCode {
    return $jf($jc(0, path.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Encryption')..add('path', path))
        .toString();
  }
}

class EncryptionBuilder implements Builder<Encryption, EncryptionBuilder> {
  _$Encryption _$v;

  String _path;
  String get path => _$this._path;
  set path(String path) => _$this._path = path;

  EncryptionBuilder();

  EncryptionBuilder get _$this {
    if (_$v != null) {
      _path = _$v.path;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Encryption other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Encryption;
  }

  @override
  void update(void Function(EncryptionBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Encryption build() {
    final _$result = _$v ?? new _$Encryption._(path: path);
    replace(_$result);
    return _$result;
  }
}

class _$Decryption extends Decryption {
  @override
  final String path;

  factory _$Decryption([void Function(DecryptionBuilder) updates]) =>
      (new DecryptionBuilder()..update(updates)).build();

  _$Decryption._({this.path}) : super._();

  @override
  Decryption rebuild(void Function(DecryptionBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  DecryptionBuilder toBuilder() => new DecryptionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Decryption && path == other.path;
  }

  @override
  int get hashCode {
    return $jf($jc(0, path.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Decryption')..add('path', path))
        .toString();
  }
}

class DecryptionBuilder implements Builder<Decryption, DecryptionBuilder> {
  _$Decryption _$v;

  String _path;
  String get path => _$this._path;
  set path(String path) => _$this._path = path;

  DecryptionBuilder();

  DecryptionBuilder get _$this {
    if (_$v != null) {
      _path = _$v.path;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Decryption other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Decryption;
  }

  @override
  void update(void Function(DecryptionBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Decryption build() {
    final _$result = _$v ?? new _$Decryption._(path: path);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
