import 'package:bloc/bloc.dart';
import 'package:struturenew/Ui/download_page/bloc/download_event.dart';
import 'package:struturenew/Ui/download_page/bloc/download_state.dart';
import 'package:struturenew/data/repository/irepository.dart';


class DownloadBloc extends Bloc<DownloadEvent, DownloadState> {
  IRepository _repository;

  DownloadBloc(this._repository) ;

  @override
  DownloadState get initialState => DownloadState.initail();

  @override
  Stream<DownloadState> mapEventToState(
      DownloadEvent event,
      ) async* {
    if (event is ClearError) {
      yield state.rebuild((b) => b..error = "");
    }
    if (event is ChangeStatus) {
      yield state.rebuild((b) => b..success = false..successdec=false);
    }
    if (event is Download) {
      try {
        yield state.rebuild((b) => b
          ..isLoading = true
          ..error = ""
            ..success=false

       );

        final date = await _repository.download2(event.dio, event.url, event.savePath);
        final encryption=await _repository.encryption(event.savePath);

        print('GetContent Success data ${date}');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = "success"
            ..success=true
            ..encryptedPath=encryption

         );

      } catch (e) {
        print('GetContent Error $e');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = "Something went wrong"
            ..success=false

          );
      }
    }
    if (event is DownloadEncr) {
      try {
        yield state.rebuild((b) => b
          ..isLoading = true
          ..error = ""
          ..successdec=false

        );

        //final date = await _repository.download2(event.dio, event.url, event.savePath);
        final encryption=await _repository.encryption(event.savePath);

       // print('GetContent Success data ${date}');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = "success"
          ..successdec=true
          ..encryptedPath=encryption

        );

      } catch (e) {
        print('GetContent Error $e');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = "Something went wrong"
          ..success=false

        );
      }
    }
    if (event is Encryption) {
      try {
        yield state.rebuild((b) => b
          ..isLoading = true
          ..error = ""
           ..success=false
        );

        final date = await _repository.encryption(event.path);
        print('GetContent Success data ${date}');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = "success"
          ..encryptedPath=date
          ..success=false
        );
      } catch (e) {
        print('GetContent Error $e');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = "Something went wrong"

          ..success=false
        );
      }
    }
    if (event is Decryption) {
      try {
        yield state.rebuild((b) => b
          ..isLoading = true
          ..error = ""
          ..successdec=false
        );

        final date = await _repository.decryption(event.path);

        print('decryp Success data ${date}');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = "success"
          ..decryPath=date
          ..successdec=true
        );
      } catch (e) {
        print('GetContent Error $e');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = "Something went wrong"

          ..successdec=false
        );
      }
    }


   }
}
