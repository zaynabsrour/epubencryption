library download_event;

import 'dart:convert';
import 'dart:io';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:dio/dio.dart';

part 'download_event.g.dart';

abstract class DownloadEvent {}

abstract class Download extends DownloadEvent
    implements Built<Download, DownloadBuilder> {
  // fields go here
Dio get dio;
  String get url;

  String get savePath;

  Download._();

  factory Download([updates(DownloadBuilder b)]) = _$Download;
}
abstract class DownloadEncr extends DownloadEvent
    implements Built<DownloadEncr, DownloadEncrBuilder> {
  // fields go here
  Dio get dio;
  String get url;

  String get savePath;

  DownloadEncr._();

  factory DownloadEncr([updates(DownloadEncrBuilder b)]) = _$DownloadEncr;
}

abstract class ClearError extends DownloadEvent
    implements Built<ClearError, ClearErrorBuilder> {
  // fields go here

  ClearError._();

  factory ClearError([updates(ClearErrorBuilder b)]) = _$ClearError;
}
abstract class ChangeStatus extends DownloadEvent
    implements Built<ChangeStatus, ChangeStatusBuilder> {
  // fields go here

  ChangeStatus._();

  factory ChangeStatus([updates(ChangeStatusBuilder b)]) = _$ChangeStatus;
}
abstract class Encryption extends DownloadEvent
    implements Built<Encryption, EncryptionBuilder> {
  // fields go here
  @nullable
  String get path;

  Encryption._();

  factory Encryption([updates(EncryptionBuilder b)]) = _$Encryption;
}
abstract class Decryption extends DownloadEvent
    implements Built<Decryption, DecryptionBuilder> {
  // fields go here
  @nullable
  String get path;

  Decryption._();

  factory Decryption([updates(DecryptionBuilder b)]) = _$Decryption;
}






