// GENERATED CODE - DO NOT MODIFY BY HAND

part of download_state;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$DownloadState extends DownloadState {
  @override
  final String error;
  @override
  final bool isLoading;
  @override
  final bool success;
  @override
  final bool successEncr;
  @override
  final bool successdec;
  @override
  final String encryptedPath;
  @override
  final String decryPath;

  factory _$DownloadState([void Function(DownloadStateBuilder) updates]) =>
      (new DownloadStateBuilder()..update(updates)).build();

  _$DownloadState._(
      {this.error,
      this.isLoading,
      this.success,
      this.successEncr,
      this.successdec,
      this.encryptedPath,
      this.decryPath})
      : super._() {
    if (error == null) {
      throw new BuiltValueNullFieldError('DownloadState', 'error');
    }
    if (isLoading == null) {
      throw new BuiltValueNullFieldError('DownloadState', 'isLoading');
    }
    if (success == null) {
      throw new BuiltValueNullFieldError('DownloadState', 'success');
    }
    if (successEncr == null) {
      throw new BuiltValueNullFieldError('DownloadState', 'successEncr');
    }
    if (successdec == null) {
      throw new BuiltValueNullFieldError('DownloadState', 'successdec');
    }
  }

  @override
  DownloadState rebuild(void Function(DownloadStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  DownloadStateBuilder toBuilder() => new DownloadStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is DownloadState &&
        error == other.error &&
        isLoading == other.isLoading &&
        success == other.success &&
        successEncr == other.successEncr &&
        successdec == other.successdec &&
        encryptedPath == other.encryptedPath &&
        decryPath == other.decryPath;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, error.hashCode), isLoading.hashCode),
                        success.hashCode),
                    successEncr.hashCode),
                successdec.hashCode),
            encryptedPath.hashCode),
        decryPath.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('DownloadState')
          ..add('error', error)
          ..add('isLoading', isLoading)
          ..add('success', success)
          ..add('successEncr', successEncr)
          ..add('successdec', successdec)
          ..add('encryptedPath', encryptedPath)
          ..add('decryPath', decryPath))
        .toString();
  }
}

class DownloadStateBuilder
    implements Builder<DownloadState, DownloadStateBuilder> {
  _$DownloadState _$v;

  String _error;
  String get error => _$this._error;
  set error(String error) => _$this._error = error;

  bool _isLoading;
  bool get isLoading => _$this._isLoading;
  set isLoading(bool isLoading) => _$this._isLoading = isLoading;

  bool _success;
  bool get success => _$this._success;
  set success(bool success) => _$this._success = success;

  bool _successEncr;
  bool get successEncr => _$this._successEncr;
  set successEncr(bool successEncr) => _$this._successEncr = successEncr;

  bool _successdec;
  bool get successdec => _$this._successdec;
  set successdec(bool successdec) => _$this._successdec = successdec;

  String _encryptedPath;
  String get encryptedPath => _$this._encryptedPath;
  set encryptedPath(String encryptedPath) =>
      _$this._encryptedPath = encryptedPath;

  String _decryPath;
  String get decryPath => _$this._decryPath;
  set decryPath(String decryPath) => _$this._decryPath = decryPath;

  DownloadStateBuilder();

  DownloadStateBuilder get _$this {
    if (_$v != null) {
      _error = _$v.error;
      _isLoading = _$v.isLoading;
      _success = _$v.success;
      _successEncr = _$v.successEncr;
      _successdec = _$v.successdec;
      _encryptedPath = _$v.encryptedPath;
      _decryPath = _$v.decryPath;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(DownloadState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$DownloadState;
  }

  @override
  void update(void Function(DownloadStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$DownloadState build() {
    final _$result = _$v ??
        new _$DownloadState._(
            error: error,
            isLoading: isLoading,
            success: success,
            successEncr: successEncr,
            successdec: successdec,
            encryptedPath: encryptedPath,
            decryPath: decryPath);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
