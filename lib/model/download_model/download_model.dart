library download_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/json_object.dart';
import 'package:built_value/serializer.dart';

import '../serializer/serializer.dart';

part 'download_model.g.dart';

abstract class DownloadModel implements Built<DownloadModel, DownloadModelBuilder> {
  // Fields
  //@nullable
  //int get id;
  //@nullable
//  String get url
  DownloadModel._();

  factory DownloadModel([void Function(DownloadModelBuilder) updates]) = _$DownloadModel;
  static Serializer<DownloadModel> get serializer => _$downloadModelSerializer;
  String toJson() {
    return json.encode(serializers.serializeWith(DownloadModel.serializer, this));
  }

  static DownloadModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        DownloadModel.serializer, json.decode(jsonString));
  }
}
