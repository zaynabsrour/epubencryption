// GENERATED CODE - DO NOT MODIFY BY HAND

part of download_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<DownloadModel> _$downloadModelSerializer =
    new _$DownloadModelSerializer();

class _$DownloadModelSerializer implements StructuredSerializer<DownloadModel> {
  @override
  final Iterable<Type> types = const [DownloadModel, _$DownloadModel];
  @override
  final String wireName = 'DownloadModel';

  @override
  Iterable<Object> serialize(Serializers serializers, DownloadModel object,
      {FullType specifiedType = FullType.unspecified}) {
    return <Object>[];
  }

  @override
  DownloadModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    return new DownloadModelBuilder().build();
  }
}

class _$DownloadModel extends DownloadModel {
  factory _$DownloadModel([void Function(DownloadModelBuilder) updates]) =>
      (new DownloadModelBuilder()..update(updates)).build();

  _$DownloadModel._() : super._();

  @override
  DownloadModel rebuild(void Function(DownloadModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  DownloadModelBuilder toBuilder() => new DownloadModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is DownloadModel;
  }

  @override
  int get hashCode {
    return 362131823;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('DownloadModel').toString();
  }
}

class DownloadModelBuilder
    implements Builder<DownloadModel, DownloadModelBuilder> {
  _$DownloadModel _$v;

  DownloadModelBuilder();

  @override
  void replace(DownloadModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$DownloadModel;
  }

  @override
  void update(void Function(DownloadModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$DownloadModel build() {
    final _$result = _$v ?? new _$DownloadModel._();
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
