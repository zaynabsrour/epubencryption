library serializer;

import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:built_collection/built_collection.dart';
import 'package:struturenew/model/base/base_response.dart';
import 'package:struturenew/model/meta/meta_model.dart';


part 'serializer.g.dart';

@SerializersFor(const [
  BaseResponse,
  MetaModel
])
final Serializers serializers = (_$serializers.toBuilder()
      ..addPlugin(StandardJsonPlugin())


    )
    .build();
