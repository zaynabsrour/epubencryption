library meta_model;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:struturenew/model/serializer/serializer.dart';

part 'meta_model.g.dart';

abstract class MetaModel implements Built<MetaModel, MetaModelBuilder> {
  // fields go here

  @BuiltValueField(wireName: "current_page")
 int get  currentPage;
  @BuiltValueField(wireName: "last_page")
  int get  lastPage;
  MetaModel._();

  factory MetaModel([updates(MetaModelBuilder b)]) = _$MetaModel;

  String toJson() {
    return json.encode(serializers.serializeWith(MetaModel.serializer, this));
  }

  static MetaModel fromJson(String jsonString) {
    return serializers.deserializeWith(MetaModel.serializer, json.decode(jsonString));
  }

  static Serializer<MetaModel> get serializer => _$metaModelSerializer;
}