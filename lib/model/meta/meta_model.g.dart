// GENERATED CODE - DO NOT MODIFY BY HAND

part of meta_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<MetaModel> _$metaModelSerializer = new _$MetaModelSerializer();

class _$MetaModelSerializer implements StructuredSerializer<MetaModel> {
  @override
  final Iterable<Type> types = const [MetaModel, _$MetaModel];
  @override
  final String wireName = 'MetaModel';

  @override
  Iterable<Object> serialize(Serializers serializers, MetaModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'current_page',
      serializers.serialize(object.currentPage,
          specifiedType: const FullType(int)),
      'last_page',
      serializers.serialize(object.lastPage,
          specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  MetaModel deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new MetaModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'current_page':
          result.currentPage = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'last_page':
          result.lastPage = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$MetaModel extends MetaModel {
  @override
  final int currentPage;
  @override
  final int lastPage;

  factory _$MetaModel([void Function(MetaModelBuilder) updates]) =>
      (new MetaModelBuilder()..update(updates)).build();

  _$MetaModel._({this.currentPage, this.lastPage}) : super._() {
    if (currentPage == null) {
      throw new BuiltValueNullFieldError('MetaModel', 'currentPage');
    }
    if (lastPage == null) {
      throw new BuiltValueNullFieldError('MetaModel', 'lastPage');
    }
  }

  @override
  MetaModel rebuild(void Function(MetaModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MetaModelBuilder toBuilder() => new MetaModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MetaModel &&
        currentPage == other.currentPage &&
        lastPage == other.lastPage;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, currentPage.hashCode), lastPage.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('MetaModel')
          ..add('currentPage', currentPage)
          ..add('lastPage', lastPage))
        .toString();
  }
}

class MetaModelBuilder implements Builder<MetaModel, MetaModelBuilder> {
  _$MetaModel _$v;

  int _currentPage;
  int get currentPage => _$this._currentPage;
  set currentPage(int currentPage) => _$this._currentPage = currentPage;

  int _lastPage;
  int get lastPage => _$this._lastPage;
  set lastPage(int lastPage) => _$this._lastPage = lastPage;

  MetaModelBuilder();

  MetaModelBuilder get _$this {
    if (_$v != null) {
      _currentPage = _$v.currentPage;
      _lastPage = _$v.lastPage;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(MetaModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$MetaModel;
  }

  @override
  void update(void Function(MetaModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$MetaModel build() {
    final _$result =
        _$v ?? new _$MetaModel._(currentPage: currentPage, lastPage: lastPage);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
