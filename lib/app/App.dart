import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
 import 'package:struturenew/Ui/download_page/page/download_page.dart';

import '../injectoin.dart';
import 'bloc/app_bloc.dart';
import 'bloc/app_event.dart';
import 'bloc/app_state.dart';

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  final _AppBloc = sl<AppBloc>();
  @override
  void initState() {
    _AppBloc.add(IniEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
        bloc: _AppBloc,
        builder: (BuildContext context, AppState state) {
           return MaterialApp(
            title: "App",
            home:  DownloadPage(),
          );
        });
  }
}
