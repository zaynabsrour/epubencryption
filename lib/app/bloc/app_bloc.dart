import 'package:bloc/bloc.dart';
import 'package:struturenew/data/repository/irepository.dart';

import 'app_event.dart';
import 'app_state.dart';

class AppBloc extends Bloc<AppEvent, AppState> {
  IRepository _repository;

  AppBloc(this._repository);

  @override
  AppState get initialState => AppState.initail();



  @override
  Stream<AppState> mapEventToState(
    AppEvent event,
  ) async* {
    if (event is IniEvent) {
      final result = await _repository.getIsLogin();
      print('LoginState : $result');
      yield state.rebuild((b) => b..loginState = result);
    }
  }
}
