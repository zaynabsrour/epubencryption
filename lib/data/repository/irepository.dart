import 'dart:io';

import 'package:built_collection/built_collection.dart';
import 'package:dio/dio.dart';



abstract class IRepository {

  Future<bool> getIsLogin();
  Future download2(Dio dio,String url,String savePath);
  Future downloadEncry(Dio dio, String url, String savePath);
  Future<String> encryption( String path);

  Future<String> decryption( String path);


}
