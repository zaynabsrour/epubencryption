import 'dart:io';
import 'dart:io';

import 'package:aes_crypt/aes_crypt.dart';
import 'package:built_collection/src/list.dart';
import 'package:dio/src/dio.dart';

import 'package:struturenew/data/http_helper/Ihttp_helper.dart';
import 'package:struturenew/data/prefs_helper/iprefs_helper.dart';
import 'package:struturenew/model/base/base_response.dart';


import 'irepository.dart';

class Repository implements IRepository {
  IHttpHelper _ihttpHelper;
 
  Repository(this._ihttpHelper);


  @override
  Future<bool> getIsLogin() async {
    return true;
  }

  @override
  Future download2(Dio dio, String url, String savePath) async {
    // TODO: implement download2
    return await _ihttpHelper.download2(dio, url, savePath);
  }
  @override
  Future downloadEncry(Dio dio, String url, String savePath) async {
    // TODO: implement download2
    return await _ihttpHelper.downloadEncry(dio, url, savePath);
  }
  @override
  Future<String> encryption( String path) async {
    // TODO: implement download2
     AesCrypt crypt = AesCrypt();
    crypt.setOverwriteMode(AesCryptOwMode.on);
    crypt.setPassword('MyPassword1234');
    String encFilepath;
    try {
      encFilepath = crypt.encryptFileSync(path);
      // setState(() {
      //   encryptionpath=encFilepath;
      // });

      print('The encryption has been completed successfully.');
      print('Encrypted file: $encFilepath');
     } catch (e) {
      if (e.type == AesCryptExceptionType.destFileExists) {
        print('The encryption has been completed unsuccessfully.');
        print(e.message);
      } else {
        return 'ERROR';
      }
    }
    return encFilepath;
  }
  @override
  Future<String> decryption( String path) async {
    String decryptionPath;
    AesCrypt crypt = AesCrypt();
    crypt.setOverwriteMode(AesCryptOwMode.on);
    crypt.setPassword('MyPassword1234');
    String decFilepath;
    try {
      decFilepath = crypt.decryptFileSync(path);
      print('The decryption has been completed successfully.');
       print('Decrypted file 1: $decFilepath');
       print('File content: ' + File(decFilepath).path);

        decryptionPath=File(decFilepath).path;

      print("dec"+decryptionPath);

    } catch (e) {
      if (e.type == AesCryptExceptionType.destFileExists) {
        print('The decryption has been completed unsuccessfully.');
         print(e.message);
      }
      else{
        return 'ERROR';
      }
    }
    return decFilepath;
  }
  ////////////////////onetoone///////////
}
