import 'dart:convert';
import 'dart:io';

import 'package:built_collection/src/list.dart';
import 'package:built_value/serializer.dart';
import 'package:dio/dio.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:flutter/foundation.dart';
import 'package:struturenew/core/constent.dart';
import 'package:struturenew/core/error/error.dart';


import 'Ihttp_helper.dart';
import 'dart:io';
import 'package:path/path.dart';
import 'package:cookie_jar/cookie_jar.dart';

class HttpHelper implements IHttpHelper {
  final Dio _dio;

  var cookieJar = CookieJar();

  HttpHelper(this._dio) {
    _dio.interceptors.add(
      LogInterceptor(
        request: true,
        responseBody: true,
        requestBody: true,
      ),
    );
    _dio.interceptors.add(CookieManager(cookieJar));
  }

  @override
  Future download2(Dio dio, String url, String savePath) async {
     var httpClient = new HttpClient();
    // TODO: implement download2
    try{

      var request = await httpClient.getUrl(Uri.parse(url)
        // url,
        // onReceiveProgress: (rec, total) async {
        //   print("Rec: $rec , Total: $total");
        //   print("Path is: "+savePath);
        //
        //
        //     downloading = await true;
        //     progressString =  ((rec / total) * 100).toStringAsFixed(0) + "%";
        //
        // },
        // //Received data with List<int>
        // options: Options(
        //     responseType: ResponseType.bytes,
        //     followRedirects: false,
        //     validateStatus: (status) {
        //       return status < 500;
        //     }),
      );
      var response = await request.close();
      File file = File(savePath);
      var bytes = await consolidateHttpClientResponseBytes(response);
      File result = await file.writeAsBytes(bytes);
      //var raf = file.openSync(mode: FileMode.write);
     // raf.writeString(response.data);



      //await raf.close();
      return progressString;
    }
    catch(e){
      print("Error is : "+e.toString());
    }


    }
  Future downloadEncry(Dio dio, String url, String savePath) async {
    var httpClient = new HttpClient();
    // TODO: implement download2
    try{

      var request = await httpClient.getUrl(Uri.parse(url)
        // url,
        // onReceiveProgress: (rec, total) async {
        //   print("Rec: $rec , Total: $total");
        //   print("Path is: "+savePath);
        //
        //
        //     downloading = await true;
        //     progressString =  ((rec / total) * 100).toStringAsFixed(0) + "%";
        //
        // },
        // //Received data with List<int>
        // options: Options(
        //     responseType: ResponseType.bytes,
        //     followRedirects: false,
        //     validateStatus: (status) {
        //       return status < 500;
        //     }),
      );
      var response = await request.close();
      File file = File(savePath);
      var bytes = await consolidateHttpClientResponseBytes(response);
      File result = await file.writeAsBytes(bytes);
      //var raf = file.openSync(mode: FileMode.write);
      // raf.writeString(response.data);



      //await raf.close();
      return progressString;
    }
    catch(e){
      print("Error is : "+e.toString());
    }


  }



}
