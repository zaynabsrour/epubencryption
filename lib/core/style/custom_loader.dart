

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'base_color.dart';


Widget loader({context}){
  return SpinKitFadingCircle(
    color: blueColor,
    size: 50.0,
  );
}

