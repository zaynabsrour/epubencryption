import 'package:flutter/material.dart';

Widget localImage({String name, double width, double height}) {
  return Image.asset(
    "assets/images/$name.png",
    fit: BoxFit.fill,
    width: width,
    height: height,
  );
}
