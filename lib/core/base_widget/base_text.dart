

 import 'package:flutter/material.dart';

Widget baseText({title,size,color,fontFamily,textAlign = TextAlign.left}){
  return Text(
    title,
    textAlign: textAlign,
    style: TextStyle(
        fontSize: size,
        fontFamily: fontFamily,
        color: color),
  );
 }