//import 'package:flutter/material.dart';
//import 'package:traveljar_part2/core/base_widget/base_text.dart';
//import 'package:traveljar_part2/core/style/base_color.dart';
//
//import 'base_click.dart';
//
//Future<void> showCustomDialog(
//    {context, content, title = "", Function onTap, clickName,icon = false}) {
//  return showDialog<void>(
//    context: context,
//
//    barrierDismissible: false, // user must tap button!
//    builder: (BuildContext context) {
//      return AlertDialog(
//        shape: RoundedRectangleBorder(
//            borderRadius: BorderRadius.all(Radius.circular(32.0))),
//        title: Text(""),
//        content: Container(
//          decoration: BoxDecoration(
//            borderRadius: BorderRadius.all(Radius.circular(20))
//          ),
//          width:double.infinity,
//          height: 120,
//          child: Column(
//            children: <Widget>[
//              Row(
//                mainAxisAlignment: MainAxisAlignment.end,
//                children: <Widget>[
//                  baseText(
//                      title: content,
//                      size: 18.0,
//                      color: blackColor,
//                      fontFamily: "SegoeUIBold"),
//                  Padding(
//                    padding: const EdgeInsets.only(bottom:17.0,left: 10),
//                    child: new RotationTransition(
//                      turns: new AlwaysStoppedAnimation(180 / 360),
//                      child:   icon ? Icon(Icons.info_outline,color: Colors.red,size: 30,) : Container()
//                    ),
//                  )
//
//                ],
//              ),
//              Padding(
//                padding: const EdgeInsets.only(top:20.0),
//                child: Row(
//                  mainAxisAlignment: MainAxisAlignment.center,
//                  children: <Widget>[
//                    baseClick(
//                        title: clickName,
//                        radius: 5,
//                        onTap: () {
//                          Navigator.pop(context);
//                          onTap();
//                        },
//                        sizeTitle: 14,
//                        height: 35,
//                        width: 110),
//                  ],
//                ),
//              ),
//            ],
//          ),
//        ),
//        actions: <Widget>[],
//      );
//    },
//  );
//}
