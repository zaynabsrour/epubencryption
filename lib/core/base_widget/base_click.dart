import 'package:flutter/material.dart';
import 'package:struturenew/core/style/base_color.dart';

Widget baseClick({Function onTap, double height = 50.0,
  double width,
  Color color = blueColor,
  double radius = 10.0,
  String title,
  double sizeTitle,
  Color colorTitle = Colors.white }) {
  return GestureDetector(
    onTap: () {
      onTap();
    },
    child: Container(
      height: height,
      width: width,
      decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.all(Radius.circular(radius))),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Center(
          child: Text(
            title,
            textAlign: TextAlign.end,
            style: TextStyle(
                fontSize: sizeTitle,
                fontFamily: "SegoeUIBold",
                color: colorTitle),
          ),
        ),
      ),
    ),
  );
}
