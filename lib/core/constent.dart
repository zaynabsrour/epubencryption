import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

const String BaseUrl = "https://talmihat.alkmal.com/api/v1/";

const String ImageUrl = "https://yalaween.com/images/img/";

const String IS_LOGIN = "login";

const String ID = "id";
const String NAME = "name";
const String PASSWORD = "password";
const String EMAIL = "email";
const String TOKEN = "token";
const String IMAGE = "image";
const String FB = "fb";
const String Linked = "linked";
const String ADDRESS = "address";
const String TW = "tw";
const String ABOUT = "about";
const String PHONE = "phone";
bool downloading = false;
var progressString = "";
String fullPath="";
var dio=Dio();
